package za.ac.tut.grpjoint.book.store.service;

import za.ac.tut.grpjoint.book.store.model.dto.SystemMenuDTO;

public interface SystemMenuService extends MyCrudService<SystemMenuDTO, Integer> {}
