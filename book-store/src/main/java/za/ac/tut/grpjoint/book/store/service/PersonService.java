package za.ac.tut.grpjoint.book.store.service;

import za.ac.tut.grpjoint.book.store.model.dto.PersonDTO;

public interface PersonService extends MyCrudService<PersonDTO, Integer> {}
