package za.ac.tut.grpjoint.book.store.model.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.ac.tut.grpjoint.book.store.model.converter.BookConverter;
import za.ac.tut.grpjoint.book.store.model.dao.jpa.BookJpaDAO;
import za.ac.tut.grpjoint.book.store.model.dto.BookDTO;
import za.ac.tut.grpjoint.book.store.model.entity.Book;

@Component 
public class BookDAO extends AbstractJpaDAO<Book, BookDTO, BookConverter, Integer, BookJpaDAO> {    

    @Autowired
    public BookDAO(BookJpaDAO jpaRepo, BookConverter converter) {
        super(jpaRepo, converter);
    }

}
