package za.ac.tut.grpjoint.book.store.model.entity;

import za.ac.tut.grpjoint.book.store.model.dto.SystemMenuDTO;
import za.ac.tut.grpjoint.book.store.model.dto.UsrRoleDTO;
import za.ac.tut.grpjoint.book.store.model.dto.PartnerAssociateDTO;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author gracematshiye
 */
@Entity
@Table(name = "PARTNER", catalog = "BOOK_STOREDB", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Partner.findAll", query = "SELECT p FROM Partner p")
    , @NamedQuery(name = "Partner.findById", query = "SELECT p FROM Partner p WHERE p.id = :id")
    , @NamedQuery(name = "Partner.findByDateCreated", query = "SELECT p FROM Partner p WHERE p.dateCreated = :dateCreated")
    , @NamedQuery(name = "Partner.findByActive", query = "SELECT p FROM Partner p WHERE p.active = :active")
    , @NamedQuery(name = "Partner.findByDateSuspended", query = "SELECT p FROM Partner p WHERE p.dateSuspended = :dateSuspended")
    , @NamedQuery(name = "Partner.findBySuspendedReason", query = "SELECT p FROM Partner p WHERE p.suspendedReason = :suspendedReason")
    , @NamedQuery(name = "Partner.findByDeleted", query = "SELECT p FROM Partner p WHERE p.deleted = :deleted")
    , @NamedQuery(name = "Partner.findByDateDeleted", query = "SELECT p FROM Partner p WHERE p.dateDeleted = :dateDeleted")
    , @NamedQuery(name = "Partner.findByCompany", query = "SELECT p FROM Partner p WHERE p.company = :company")
    , @NamedQuery(name = "Partner.findByUuid", query = "SELECT p FROM Partner p WHERE p.uuid = :uuid")})
public class Partner implements Serializable, MyEntity {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "date_created", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreated;
    @Basic(optional = false)
    @NotNull
    @Column(name = "active", nullable = false)
    private boolean active;
    @Column(name = "date_suspended")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateSuspended;
    @Size(max = 450)
    @Column(name = "suspended_reason", length = 450)
    private String suspendedReason;
    @Column(name = "deleted")
    private Boolean deleted;
    @Column(name = "date_deleted")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateDeleted;
    @Column(name = "company")
    private Integer company;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 36)
    @Column(name = "uuid", nullable = false, length = 36)
    private String uuid;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "partner1")
    private List<PartnerAssociateDTO> partnerAssociateList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "partner")
    private List<UsrRoleDTO> usrRoleList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "partner")
    private List<SystemMenuDTO> systemMenuList;

    public Partner() {
    }

    public Partner(Integer id) {
        this.id = id;
    }

    public Partner(Integer id, Date dateCreated, boolean active, String uuid) {
        this.id = id;
        this.dateCreated = dateCreated;
        this.active = active;
        this.uuid = uuid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public boolean getActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Date getDateSuspended() {
        return dateSuspended;
    }

    public void setDateSuspended(Date dateSuspended) {
        this.dateSuspended = dateSuspended;
    }

    public String getSuspendedReason() {
        return suspendedReason;
    }

    public void setSuspendedReason(String suspendedReason) {
        this.suspendedReason = suspendedReason;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Date getDateDeleted() {
        return dateDeleted;
    }

    public void setDateDeleted(Date dateDeleted) {
        this.dateDeleted = dateDeleted;
    }

    public Integer getCompany() {
        return company;
    }

    public void setCompany(Integer company) {
        this.company = company;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @XmlTransient
    public List<PartnerAssociateDTO> getPartnerAssociateList() {
        return partnerAssociateList;
    }

    public void setPartnerAssociateList(List<PartnerAssociateDTO> partnerAssociateList) {
        this.partnerAssociateList = partnerAssociateList;
    }

    @XmlTransient
    public List<UsrRoleDTO> getUsrRoleList() {
        return usrRoleList;
    }

    public void setUsrRoleList(List<UsrRoleDTO> usrRoleList) {
        this.usrRoleList = usrRoleList;
    }

    @XmlTransient
    public List<SystemMenuDTO> getSystemMenuList() {
        return systemMenuList;
    }

    public void setSystemMenuList(List<SystemMenuDTO> systemMenuList) {
        this.systemMenuList = systemMenuList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Partner)) {
            return false;
        }
        Partner other = (Partner) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "za.ac.tut.grpjoint.book.store.model.entity.Partner[ id=" + id + " ]";
    }
    
}
