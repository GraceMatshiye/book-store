package za.ac.tut.grpjoint.book.store.service;

import za.ac.tut.grpjoint.book.store.model.dto.UsrRoleDTO;

public interface UsrRoleService extends MyCrudService<UsrRoleDTO, Integer> {}
