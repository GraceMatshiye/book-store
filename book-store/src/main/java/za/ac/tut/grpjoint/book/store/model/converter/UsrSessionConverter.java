package za.ac.tut.grpjoint.book.store.model.converter;

import org.modelmapper.ModelMapper;
import za.ac.tut.grpjoint.book.store.model.dto.UsrSessionDTO;
import za.ac.tut.grpjoint.book.store.model.entity.UsrSession;

public class UsrSessionConverter extends AbstractConverter<UsrSession, UsrSessionDTO> {

    @Override
    public UsrSession toEntity(UsrSessionDTO dto) {
        ModelMapper mm =  new ModelMapper();
        UsrSession entity = mm.map(dto, UsrSession.class);
        return entity;
    }

    @Override
    public UsrSessionDTO toDTO(UsrSession entity) {
        ModelMapper mm =  new ModelMapper();
        UsrSessionDTO dto = mm.map(entity, UsrSessionDTO.class);
        return dto;
    }
}
