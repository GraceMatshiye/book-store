package za.ac.tut.grpjoint.book.store.model.dao;

import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import za.ac.tut.grpjoint.book.store.model.converter.MyConverter;
import za.ac.tut.grpjoint.book.store.model.dto.MyDTO;
import za.ac.tut.grpjoint.book.store.model.entity.MyEntity;
import za.ac.tut.grpjoint.book.store.service.exception.RecordNotCreatedException;
import za.ac.tut.grpjoint.book.store.service.exception.RecordNotUpdatedException;

/**
 *
 * @author gracematshiye
 */
public abstract class AbstractJpaDAO<
        ENTITY extends MyEntity, DTO extends MyDTO, CONVERTER extends MyConverter<ENTITY, DTO>, ID extends Serializable, R extends JpaRepository<ENTITY, ID>> implements MyDAO<ENTITY, DTO, CONVERTER, ID> {

    private final R jpaRepo;
    private final CONVERTER converter;

    public AbstractJpaDAO(final R jpaRepo, CONVERTER converter) {
        this.jpaRepo = jpaRepo;
        this.converter = converter;
    }

    @Override
    public List<DTO> findAll() {
        List<ENTITY> entities = jpaRepo.findAll();
        return converter.toDTO(entities);
    }

    @Override
    public DTO findById(ID id) {
        ENTITY entity = jpaRepo.findOne(id);
        return converter.toDTO(entity);
    }

    @Override
    public DTO save(DTO dto) throws RecordNotCreatedException {
        ENTITY entity = converter.toEntity(dto);
        entity = jpaRepo.save(entity);
        return converter.toDTO(entity);
    }

    @Override
    public void save(List<DTO> dtoList) throws RecordNotCreatedException {
        List<ENTITY> entities = converter.toEntity(dtoList);
        jpaRepo.save(entities);
    }

    @Override
    public DTO update(DTO dto) throws RecordNotUpdatedException {
        ENTITY entity = converter.toEntity(dto);
        entity = jpaRepo.save(entity);
        return converter.toDTO(entity);
    }

    @Override
    public void delete(DTO dto) throws RecordNotUpdatedException {
        ENTITY entity = converter.toEntity(dto);
        jpaRepo.delete(entity);
    }

}
