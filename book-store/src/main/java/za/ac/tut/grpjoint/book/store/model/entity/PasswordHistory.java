package za.ac.tut.grpjoint.book.store.model.entity;

import za.ac.tut.grpjoint.book.store.model.dto.UsrDTO;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author gracematshiye
 */
@Entity
@Table(name = "PASSWORD_HISTORY", catalog = "BOOK_STOREDB", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PasswordHistory.findAll", query = "SELECT p FROM PasswordHistory p")
    , @NamedQuery(name = "PasswordHistory.findById", query = "SELECT p FROM PasswordHistory p WHERE p.id = :id")
    , @NamedQuery(name = "PasswordHistory.findByPasswd", query = "SELECT p FROM PasswordHistory p WHERE p.passwd = :passwd")
    , @NamedQuery(name = "PasswordHistory.findByDateCreated", query = "SELECT p FROM PasswordHistory p WHERE p.dateCreated = :dateCreated")
    , @NamedQuery(name = "PasswordHistory.findByActive", query = "SELECT p FROM PasswordHistory p WHERE p.active = :active")
    , @NamedQuery(name = "PasswordHistory.findByUuid", query = "SELECT p FROM PasswordHistory p WHERE p.uuid = :uuid")})
public class PasswordHistory implements Serializable, MyEntity {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 450)
    @Column(name = "passwd", nullable = false, length = 450)
    private String passwd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "date_created", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreated;
    @Basic(optional = false)
    @NotNull
    @Column(name = "active", nullable = false)
    private boolean active;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 36)
    @Column(name = "uuid", nullable = false, length = 36)
    private String uuid;
    @JoinColumn(name = "usr", referencedColumnName = "id")
    @ManyToOne
    private UsrDTO usr;

    public PasswordHistory() {
    }

    public PasswordHistory(Integer id) {
        this.id = id;
    }

    public PasswordHistory(Integer id, String passwd, Date dateCreated, boolean active, String uuid) {
        this.id = id;
        this.passwd = passwd;
        this.dateCreated = dateCreated;
        this.active = active;
        this.uuid = uuid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public boolean getActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public UsrDTO getUsr() {
        return usr;
    }

    public void setUsr(UsrDTO usr) {
        this.usr = usr;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PasswordHistory)) {
            return false;
        }
        PasswordHistory other = (PasswordHistory) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "za.ac.tut.grpjoint.book.store.model.entity.PasswordHistory[ id=" + id + " ]";
    }
    
}
