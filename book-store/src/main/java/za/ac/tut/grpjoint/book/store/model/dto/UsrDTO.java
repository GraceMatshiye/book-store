/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.ac.tut.grpjoint.book.store.model.dto;

import java.util.Date;
import java.util.List;
import lombok.Data;

/**
 *
 * @author gracematshiye
 */
@Data
public class UsrDTO implements MyDTO {

    private Integer id;
    private String username;
    private Date lastLoginDate;
    private Boolean suspended;
    private String suspendedReason;
    private boolean passwordChangeRequired;
    private Date passwordChangeDate;
    private String uuid;
    private List<PartnerAssociateDTO> partnerAssociateList;
    private PersonDTO person;
    private UsrRoleDTO usrRole;
    private List<BookShelfDTO> bookShelfList;
    private List<UsrSessionDTO> usrSessionList;
    private List<PasswordHistoryDTO> passwordHistoryList;

}
