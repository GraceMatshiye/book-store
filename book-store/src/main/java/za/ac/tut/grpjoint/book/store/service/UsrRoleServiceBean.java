package za.ac.tut.grpjoint.book.store.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.ac.tut.grpjoint.book.store.model.dao.UsrRoleDAO;
import za.ac.tut.grpjoint.book.store.model.dto.UsrRoleDTO;

@Component 
public class UsrRoleServiceBean extends AbstractCrudService<UsrRoleDTO, Integer, UsrRoleDAO>implements UsrRoleService {

    @Autowired
    public UsrRoleServiceBean(UsrRoleDAO dao) {
        super(dao);
    }
}
