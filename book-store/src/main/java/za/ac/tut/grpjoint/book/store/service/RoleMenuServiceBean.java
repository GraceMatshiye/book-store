package za.ac.tut.grpjoint.book.store.service;

import org.springframework.stereotype.Component;
import za.ac.tut.grpjoint.book.store.model.dao.RoleMenuDAO;
import za.ac.tut.grpjoint.book.store.model.dto.RoleMenuDTO;

@Component 
public class RoleMenuServiceBean extends AbstractCrudService<RoleMenuDTO, Integer, RoleMenuDAO>implements RoleMenuService {

    public RoleMenuServiceBean(RoleMenuDAO dao) {
        super(dao);
    }
}
