package za.ac.tut.grpjoint.book.store.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.ac.tut.grpjoint.book.store.model.dao.CategoryDAO;
import za.ac.tut.grpjoint.book.store.model.dto.CategoryDTO;

@Component
public class CategoryServiceBean extends AbstractCrudService<CategoryDTO, Integer, CategoryDAO>implements CategoryService {

    @Autowired
    public CategoryServiceBean(CategoryDAO dao) {
        super(dao);
    }
}
