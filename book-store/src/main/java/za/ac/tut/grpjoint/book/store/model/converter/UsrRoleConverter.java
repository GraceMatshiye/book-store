package za.ac.tut.grpjoint.book.store.model.converter;

import org.modelmapper.ModelMapper;
import za.ac.tut.grpjoint.book.store.model.dto.UsrRoleDTO;
import za.ac.tut.grpjoint.book.store.model.entity.UsrRole;

public class UsrRoleConverter extends AbstractConverter<UsrRole, UsrRoleDTO> {

    @Override
    public UsrRole toEntity(UsrRoleDTO dto) {
        ModelMapper mm =  new ModelMapper();
        UsrRole entity = mm.map(dto, UsrRole.class);
        return entity;
    }

    @Override
    public UsrRoleDTO toDTO(UsrRole entity) {
        ModelMapper mm =  new ModelMapper();
        UsrRoleDTO dto = mm.map(entity, UsrRoleDTO.class);
        return dto;
    }
}
