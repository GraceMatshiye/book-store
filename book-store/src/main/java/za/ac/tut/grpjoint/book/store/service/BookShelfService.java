package za.ac.tut.grpjoint.book.store.service;

import za.ac.tut.grpjoint.book.store.model.dto.BookShelfDTO;

public interface BookShelfService extends MyCrudService<BookShelfDTO, Integer> {}
