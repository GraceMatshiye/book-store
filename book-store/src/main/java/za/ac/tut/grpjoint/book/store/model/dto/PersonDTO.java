/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.ac.tut.grpjoint.book.store.model.dto;

import java.util.List;
import lombok.Data;

/**
 *
 * @author gracematshiye
 */
@Data
public class PersonDTO implements MyDTO {

    private Integer id;
    private String title;
    private String initials;
    private String firstName;
    private String otherName;
    private String surname;
    private String email;
    private String cellNum;
    private String telNum;
    private String otherNum;
    private String uuid;
    private List<BookDTO> bookList;
    private List<UsrDTO> usrList;

}
