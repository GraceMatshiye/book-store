package za.ac.tut.grpjoint.book.store.model.converter;

import java.util.List;
import lombok.NonNull;
import za.ac.tut.grpjoint.book.store.model.dto.MyDTO;
import za.ac.tut.grpjoint.book.store.model.entity.MyEntity;

/**
 *
 * @author gracematshiye
 */
public interface MyConverter<E extends MyEntity, D extends MyDTO> {

    E toEntity(@NonNull D dto);

    D toDTO(@NonNull E entity);

    List<E> toEntity(@NonNull List<D> dtos);

    List<D> toDTO(@NonNull List<E> entities);
}
