/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.ac.tut.grpjoint.book.store.model.dto;

import java.util.Date;
import java.util.List;
import lombok.Data;

/**
 *
 * @author gracematshiye
 */
@Data
public class PartnerDTO implements MyDTO {

    private Integer id;
    private Date dateCreated;
    private boolean active;
    private Date dateSuspended;
    private String suspendedReason;
    private Boolean deleted;
    private Date dateDeleted;
    private Integer company;
    private String uuid;
    private List<PartnerAssociateDTO> partnerAssociateList;
    private List<UsrRoleDTO> usrRoleList;
    private List<SystemMenuDTO> systemMenuList;

}
