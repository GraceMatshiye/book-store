package za.ac.tut.grpjoint.book.store.service;

import za.ac.tut.grpjoint.book.store.model.dto.UsrDTO;

public interface UsrService extends MyCrudService<UsrDTO, Integer> {}
