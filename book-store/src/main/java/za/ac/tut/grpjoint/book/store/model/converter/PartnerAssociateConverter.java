package za.ac.tut.grpjoint.book.store.model.converter;

import org.modelmapper.ModelMapper;
import za.ac.tut.grpjoint.book.store.model.dto.PartnerAssociateDTO;
import za.ac.tut.grpjoint.book.store.model.entity.PartnerAssociate;

public class PartnerAssociateConverter extends AbstractConverter<PartnerAssociate, PartnerAssociateDTO> {

     @Override
    public PartnerAssociate toEntity(PartnerAssociateDTO dto) {
        ModelMapper mm =  new ModelMapper();
        PartnerAssociate entity = mm.map(dto, PartnerAssociate.class);
        return entity;
    }

    @Override
    public PartnerAssociateDTO toDTO(PartnerAssociate entity) {
        ModelMapper mm =  new ModelMapper();
        PartnerAssociateDTO dto = mm.map(entity, PartnerAssociateDTO.class);
        return dto;
    }
}
