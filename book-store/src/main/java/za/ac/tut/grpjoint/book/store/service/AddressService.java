package za.ac.tut.grpjoint.book.store.service;

import za.ac.tut.grpjoint.book.store.model.dto.AddressDTO;

public interface AddressService extends MyCrudService<AddressDTO, Integer> {
}
