package za.ac.tut.grpjoint.book.store.model.entity;

import za.ac.tut.grpjoint.book.store.model.dto.PartnerDTO;
import za.ac.tut.grpjoint.book.store.model.dto.RoleMenuDTO;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author gracematshiye
 */
@Entity
@Table(name = "SYSTEM_MENU", catalog = "BOOK_STOREDB", schema = "", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"uuid"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SystemMenu.findAll", query = "SELECT s FROM SystemMenu s")
    , @NamedQuery(name = "SystemMenu.findById", query = "SELECT s FROM SystemMenu s WHERE s.id = :id")
    , @NamedQuery(name = "SystemMenu.findByDisplayName", query = "SELECT s FROM SystemMenu s WHERE s.displayName = :displayName")
    , @NamedQuery(name = "SystemMenu.findByIconClass", query = "SELECT s FROM SystemMenu s WHERE s.iconClass = :iconClass")
    , @NamedQuery(name = "SystemMenu.findByHref", query = "SELECT s FROM SystemMenu s WHERE s.href = :href")
    , @NamedQuery(name = "SystemMenu.findByDescription", query = "SELECT s FROM SystemMenu s WHERE s.description = :description")
    , @NamedQuery(name = "SystemMenu.findByActive", query = "SELECT s FROM SystemMenu s WHERE s.active = :active")
    , @NamedQuery(name = "SystemMenu.findByMenuOrder", query = "SELECT s FROM SystemMenu s WHERE s.menuOrder = :menuOrder")
    , @NamedQuery(name = "SystemMenu.findBySystemModule", query = "SELECT s FROM SystemMenu s WHERE s.systemModule = :systemModule")
    , @NamedQuery(name = "SystemMenu.findByUuid", query = "SELECT s FROM SystemMenu s WHERE s.uuid = :uuid")})
public class SystemMenu implements Serializable, MyEntity {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "display_name", nullable = false, length = 45)
    private String displayName;
    @Size(max = 120)
    @Column(name = "icon_class", length = 120)
    private String iconClass;
    @Size(max = 450)
    @Column(name = "href", length = 450)
    private String href;
    @Size(max = 450)
    @Column(name = "description", length = 450)
    private String description;
    @Basic(optional = false)
    @NotNull
    @Column(name = "active", nullable = false)
    private boolean active;
    @Basic(optional = false)
    @NotNull
    @Column(name = "menu_order", nullable = false)
    private int menuOrder;
    @Basic(optional = false)
    @NotNull
    @Column(name = "system_module", nullable = false)
    private int systemModule;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 36)
    @Column(name = "uuid", nullable = false, length = 36)
    private String uuid;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "mnu")
    private List<RoleMenuDTO> roleMenuList;
    @OneToMany(mappedBy = "parent")
    private List<SystemMenu> systemMenuList;
    @JoinColumn(name = "parent", referencedColumnName = "id")
    @ManyToOne
    private SystemMenu parent;
    @JoinColumn(name = "partner", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private PartnerDTO partner;

    public SystemMenu() {
    }

    public SystemMenu(Integer id) {
        this.id = id;
    }

    public SystemMenu(Integer id, String displayName, boolean active, int menuOrder, int systemModule, String uuid) {
        this.id = id;
        this.displayName = displayName;
        this.active = active;
        this.menuOrder = menuOrder;
        this.systemModule = systemModule;
        this.uuid = uuid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getIconClass() {
        return iconClass;
    }

    public void setIconClass(String iconClass) {
        this.iconClass = iconClass;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean getActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public int getMenuOrder() {
        return menuOrder;
    }

    public void setMenuOrder(int menuOrder) {
        this.menuOrder = menuOrder;
    }

    public int getSystemModule() {
        return systemModule;
    }

    public void setSystemModule(int systemModule) {
        this.systemModule = systemModule;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @XmlTransient
    public List<RoleMenuDTO> getRoleMenuList() {
        return roleMenuList;
    }

    public void setRoleMenuList(List<RoleMenuDTO> roleMenuList) {
        this.roleMenuList = roleMenuList;
    }

    @XmlTransient
    public List<SystemMenu> getSystemMenuList() {
        return systemMenuList;
    }

    public void setSystemMenuList(List<SystemMenu> systemMenuList) {
        this.systemMenuList = systemMenuList;
    }

    public SystemMenu getParent() {
        return parent;
    }

    public void setParent(SystemMenu parent) {
        this.parent = parent;
    }

    public PartnerDTO getPartner() {
        return partner;
    }

    public void setPartner(PartnerDTO partner) {
        this.partner = partner;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SystemMenu)) {
            return false;
        }
        SystemMenu other = (SystemMenu) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "za.ac.tut.grpjoint.book.store.model.entity.SystemMenu[ id=" + id + " ]";
    }
    
}
