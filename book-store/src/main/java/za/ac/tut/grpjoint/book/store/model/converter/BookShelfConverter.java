package za.ac.tut.grpjoint.book.store.model.converter;

import org.modelmapper.ModelMapper;
import za.ac.tut.grpjoint.book.store.model.dto.BookShelfDTO;
import za.ac.tut.grpjoint.book.store.model.entity.BookShelf;

public class BookShelfConverter extends AbstractConverter<BookShelf, BookShelfDTO> {

    @Override
    public BookShelf toEntity(BookShelfDTO dto) {
        ModelMapper mm = new ModelMapper();
        BookShelf entity = mm.map(dto, BookShelf.class);
        return entity;
    }

    @Override
    public BookShelfDTO toDTO(BookShelf entity) {
        ModelMapper mm = new ModelMapper();
        BookShelfDTO dto = mm.map(entity, BookShelfDTO.class);
        return dto;
    }
}
