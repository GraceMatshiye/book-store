package za.ac.tut.grpjoint.book.store.model.converter;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import za.ac.tut.grpjoint.book.store.model.dto.BookDTO;
import za.ac.tut.grpjoint.book.store.model.entity.Book;

@Component
public class BookConverter extends AbstractConverter<Book, BookDTO> {

     @Override
    public Book toEntity(BookDTO dto) {
        ModelMapper mm =  new ModelMapper();
        Book entity = mm.map(dto, Book.class);
        return entity;
    }

    @Override
    public BookDTO toDTO(Book entity) {
        ModelMapper mm =  new ModelMapper();
        BookDTO dto = mm.map(entity, BookDTO.class);
        return dto;
    }
}
