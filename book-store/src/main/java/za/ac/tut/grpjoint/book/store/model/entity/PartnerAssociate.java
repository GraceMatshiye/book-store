package za.ac.tut.grpjoint.book.store.model.entity;

import za.ac.tut.grpjoint.book.store.model.dto.PartnerDTO;
import za.ac.tut.grpjoint.book.store.model.dto.PartnerAssociatePKDTO;
import za.ac.tut.grpjoint.book.store.model.dto.UsrDTO;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author gracematshiye
 */
@Entity
@Table(name = "PARTNER_ASSOCIATE", catalog = "BOOK_STOREDB", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PartnerAssociate.findAll", query = "SELECT p FROM PartnerAssociate p")
    , @NamedQuery(name = "PartnerAssociate.findByPartner", query = "SELECT p FROM PartnerAssociate p WHERE p.partnerAssociatePK.partner = :partner")
    , @NamedQuery(name = "PartnerAssociate.findByAssociate", query = "SELECT p FROM PartnerAssociate p WHERE p.partnerAssociatePK.associate = :associate")
    , @NamedQuery(name = "PartnerAssociate.findByDateCreated", query = "SELECT p FROM PartnerAssociate p WHERE p.dateCreated = :dateCreated")
    , @NamedQuery(name = "PartnerAssociate.findByUuid", query = "SELECT p FROM PartnerAssociate p WHERE p.uuid = :uuid")})
public class PartnerAssociate implements Serializable, MyEntity {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PartnerAssociatePKDTO partnerAssociatePK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "date_created", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreated;
    @Size(max = 36)
    @Column(name = "uuid", length = 36)
    private String uuid;
    @JoinColumn(name = "created_by", referencedColumnName = "id")
    @ManyToOne
    private UsrDTO createdBy;
    @JoinColumn(name = "partner", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private PartnerDTO partner1;

    public PartnerAssociate() {
    }

    public PartnerAssociate(PartnerAssociatePKDTO partnerAssociatePK) {
        this.partnerAssociatePK = partnerAssociatePK;
    }

    public PartnerAssociate(PartnerAssociatePKDTO partnerAssociatePK, Date dateCreated) {
        this.partnerAssociatePK = partnerAssociatePK;
        this.dateCreated = dateCreated;
    }

    public PartnerAssociate(int partner, int associate) {
        this.partnerAssociatePK = new PartnerAssociatePKDTO(partner, associate);
    }

    public PartnerAssociatePKDTO getPartnerAssociatePK() {
        return partnerAssociatePK;
    }

    public void setPartnerAssociatePK(PartnerAssociatePKDTO partnerAssociatePK) {
        this.partnerAssociatePK = partnerAssociatePK;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public UsrDTO getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(UsrDTO createdBy) {
        this.createdBy = createdBy;
    }

    public PartnerDTO getPartner1() {
        return partner1;
    }

    public void setPartner1(PartnerDTO partner1) {
        this.partner1 = partner1;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (partnerAssociatePK != null ? partnerAssociatePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PartnerAssociate)) {
            return false;
        }
        PartnerAssociate other = (PartnerAssociate) object;
        if ((this.partnerAssociatePK == null && other.partnerAssociatePK != null) || (this.partnerAssociatePK != null && !this.partnerAssociatePK.equals(other.partnerAssociatePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "za.ac.tut.grpjoint.book.store.model.entity.PartnerAssociate[ partnerAssociatePK=" + partnerAssociatePK + " ]";
    }
    
}
