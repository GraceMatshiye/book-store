/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.ac.tut.grpjoint.book.store.model.dto;

import java.util.List;
import lombok.Data;

/**
 *
 * @author gracematshiye
 */
@Data
public class CountryDTO implements MyDTO {

    private String code;
    private String name;
    private String continent;
    private String region;
    private float surfaceArea;
    private String localName;
    private String governmentForm;
    private Integer capital;
    private String code2;
    private Integer dialingCode;
    private String uuid;
    private List<CityDTO> cityList;

}
