package za.ac.tut.grpjoint.book.store.model.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.ac.tut.grpjoint.book.store.model.converter.CategoryConverter;
import za.ac.tut.grpjoint.book.store.model.dao.jpa.CategoryJpaDAO;
import za.ac.tut.grpjoint.book.store.model.dto.CategoryDTO;
import za.ac.tut.grpjoint.book.store.model.entity.Category;

@Component 
public class CategoryDAO extends AbstractJpaDAO<Category, CategoryDTO, CategoryConverter, Integer, CategoryJpaDAO> {

    @Autowired
    public CategoryDAO(CategoryJpaDAO jpaRepo, CategoryConverter converter) {
        super(jpaRepo, converter);
    }
}
