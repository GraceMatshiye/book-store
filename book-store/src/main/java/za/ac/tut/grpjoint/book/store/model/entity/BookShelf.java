package za.ac.tut.grpjoint.book.store.model.entity;

import za.ac.tut.grpjoint.book.store.model.dto.UsrDTO;
import za.ac.tut.grpjoint.book.store.model.dto.BookDTO;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author gracematshiye
 */
@Entity
@Table(name = "BOOK_SHELF", catalog = "BOOK_STOREDB", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BookShelf.findAll", query = "SELECT b FROM BookShelf b")
    , @NamedQuery(name = "BookShelf.findById", query = "SELECT b FROM BookShelf b WHERE b.id = :id")
    , @NamedQuery(name = "BookShelf.findBySellingPrice", query = "SELECT b FROM BookShelf b WHERE b.sellingPrice = :sellingPrice")
    , @NamedQuery(name = "BookShelf.findByDateListed", query = "SELECT b FROM BookShelf b WHERE b.dateListed = :dateListed")
    , @NamedQuery(name = "BookShelf.findByStillAvailable", query = "SELECT b FROM BookShelf b WHERE b.stillAvailable = :stillAvailable")})
public class BookShelf implements Serializable, MyEntity {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "selling_price", nullable = false)
    private double sellingPrice;
    @Basic(optional = false)
    @NotNull
    @Column(name = "date_listed", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateListed;
    @Basic(optional = false)
    @NotNull
    @Column(name = "still_available", nullable = false)
    private boolean stillAvailable;
    @JoinColumn(name = "book", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private BookDTO book;
    @JoinColumn(name = "seller", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private UsrDTO seller;

    public BookShelf() {
    }

    public BookShelf(Integer id) {
        this.id = id;
    }

    public BookShelf(Integer id, double sellingPrice, Date dateListed, boolean stillAvailable) {
        this.id = id;
        this.sellingPrice = sellingPrice;
        this.dateListed = dateListed;
        this.stillAvailable = stillAvailable;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public double getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(double sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    public Date getDateListed() {
        return dateListed;
    }

    public void setDateListed(Date dateListed) {
        this.dateListed = dateListed;
    }

    public boolean getStillAvailable() {
        return stillAvailable;
    }

    public void setStillAvailable(boolean stillAvailable) {
        this.stillAvailable = stillAvailable;
    }

    public BookDTO getBook() {
        return book;
    }

    public void setBook(BookDTO book) {
        this.book = book;
    }

    public UsrDTO getSeller() {
        return seller;
    }

    public void setSeller(UsrDTO seller) {
        this.seller = seller;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BookShelf)) {
            return false;
        }
        BookShelf other = (BookShelf) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "za.ac.tut.grpjoint.book.store.model.entity.BookShelf[ id=" + id + " ]";
    }
    
}
