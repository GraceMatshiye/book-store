package za.ac.tut.grpjoint.book.store.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.ac.tut.grpjoint.book.store.model.dao.UsrSessionDAO;
import za.ac.tut.grpjoint.book.store.model.dto.UsrSessionDTO;

@Component 
public class UsrSessionServiceBean extends AbstractCrudService<UsrSessionDTO, Integer, UsrSessionDAO>implements UsrSessionService {

    @Autowired
    public UsrSessionServiceBean(UsrSessionDAO dao) {
        super(dao);
    }
}
