package za.ac.tut.grpjoint.book.store.model.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.ac.tut.grpjoint.book.store.model.converter.UsrRoleConverter;
import za.ac.tut.grpjoint.book.store.model.dao.jpa.UsrRoleJpaDAO;
import za.ac.tut.grpjoint.book.store.model.dto.UsrRoleDTO;
import za.ac.tut.grpjoint.book.store.model.entity.UsrRole;

@Component
public class UsrRoleDAO extends AbstractJpaDAO<UsrRole, UsrRoleDTO, UsrRoleConverter, Integer, UsrRoleJpaDAO> {

    @Autowired
    public UsrRoleDAO(UsrRoleJpaDAO jpaRepo, UsrRoleConverter converter) {
        super(jpaRepo, converter);
    }
}
