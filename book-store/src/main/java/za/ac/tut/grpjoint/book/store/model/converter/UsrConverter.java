package za.ac.tut.grpjoint.book.store.model.converter;

import org.modelmapper.ModelMapper;
import za.ac.tut.grpjoint.book.store.model.dto.UsrDTO;
import za.ac.tut.grpjoint.book.store.model.entity.Usr;

public class UsrConverter extends AbstractConverter<Usr, UsrDTO> {

    @Override
    public Usr toEntity(UsrDTO dto) {
        ModelMapper mm =  new ModelMapper();
        Usr entity = mm.map(dto, Usr.class);
        return entity;
    }

    @Override
    public UsrDTO toDTO(Usr entity) {
        ModelMapper mm =  new ModelMapper();
        UsrDTO dto = mm.map(entity, UsrDTO.class);
        return dto;
    }
}
