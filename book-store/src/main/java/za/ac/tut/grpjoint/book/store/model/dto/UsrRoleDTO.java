/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.ac.tut.grpjoint.book.store.model.dto;

import java.util.List;
import lombok.Data;

/**
 *
 * @author gracematshiye
 */
@Data
public class UsrRoleDTO implements MyDTO {

    private Integer id;
    private String name;
    private String systemKey;
    private String description;
    private int roleLevel;
    private boolean active;
    private String uuid;
    private List<UsrDTO> usrList;
    private PartnerDTO partner;
    private List<RoleMenuDTO> roleMenuList;

}
