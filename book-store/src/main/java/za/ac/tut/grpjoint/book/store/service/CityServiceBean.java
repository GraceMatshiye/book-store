package za.ac.tut.grpjoint.book.store.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.ac.tut.grpjoint.book.store.model.dao.CityDAO;
import za.ac.tut.grpjoint.book.store.model.dto.CityDTO;

@Component
public class CityServiceBean extends AbstractCrudService<CityDTO, Integer, CityDAO>implements CityService {

    @Autowired
    public CityServiceBean(CityDAO dao) {
        super(dao);
    }
}
