/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.ac.tut.grpjoint.book.store.model.dto;

import java.util.Date;
import lombok.Data;

/**
 *
 * @author gracematshiye
 */
@Data
public class BookShelfDTO implements MyDTO {

    private Integer id;
    private double sellingPrice;
    private Date dateListed;
    private boolean stillAvailable;
    private BookDTO book;
    private UsrDTO seller;

}
