/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.ac.tut.grpjoint.book.store.model.dto;

import java.util.Date;
import java.util.List;
import lombok.Data;

/**
 *
 * @author gracematshiye
 */
@Data
public class BookDTO implements MyDTO {

    private Integer id;
    private String title;
    private String description;
    private String isbn;
    private String edition;
    private String language;
    private String additionalInfo;
    private Date datePublished;
    private List<PersonDTO> personList;
    private CategoryDTO category;
    private List<BookShelfDTO> bookShelfList;

}
