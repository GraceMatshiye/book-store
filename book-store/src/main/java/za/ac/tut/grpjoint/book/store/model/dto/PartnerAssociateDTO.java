/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.ac.tut.grpjoint.book.store.model.dto;

import java.util.Date;
import lombok.Data;

/**
 *
 * @author gracematshiye
 */
@Data
public class PartnerAssociateDTO implements MyDTO {

    protected PartnerAssociatePKDTO partnerAssociatePK;
    private Date dateCreated;
    private String uuid;
    private UsrDTO createdBy;
    private PartnerDTO partner1;

}
