package za.ac.tut.grpjoint.book.store.model.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.ac.tut.grpjoint.book.store.model.converter.PartnerAssociateConverter;
import za.ac.tut.grpjoint.book.store.model.dao.jpa.PartnerAssociateJpaDAO;
import za.ac.tut.grpjoint.book.store.model.dto.PartnerAssociateDTO;
import za.ac.tut.grpjoint.book.store.model.entity.PartnerAssociate;

@Component
public class PartnerAssociateDAO extends AbstractJpaDAO<PartnerAssociate, PartnerAssociateDTO, PartnerAssociateConverter, Integer, PartnerAssociateJpaDAO> {

    @Autowired
    public PartnerAssociateDAO(PartnerAssociateJpaDAO jpaRepo, PartnerAssociateConverter converter) {
        super(jpaRepo, converter);
    }
}
