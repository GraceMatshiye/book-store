package za.ac.tut.grpjoint.book.store.model.dto;

import lombok.Data;

/**
 *
 * @author gracematshiye
 */
@Data
public class AddressDTO implements MyDTO {

    private Integer id;
    private String line1;
    private String line2;
    private String postalCode;
    private Float gpsLatitude;
    private Float gpsLongitude;
    private String uuid;
    private CityDTO city;
    private SuburbDTO suburb;

}
