package za.ac.tut.grpjoint.book.store.model.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.ac.tut.grpjoint.book.store.model.converter.SuburbConverter;
import za.ac.tut.grpjoint.book.store.model.dao.jpa.SuburbJpaDAO;
import za.ac.tut.grpjoint.book.store.model.dto.SuburbDTO;
import za.ac.tut.grpjoint.book.store.model.entity.Suburb;

@Component
public class SuburbDAO extends AbstractJpaDAO<Suburb, SuburbDTO, SuburbConverter, Integer, SuburbJpaDAO> {

    @Autowired
    public SuburbDAO(SuburbJpaDAO jpaRepo, SuburbConverter converter) {
        super(jpaRepo, converter);
    }
}
