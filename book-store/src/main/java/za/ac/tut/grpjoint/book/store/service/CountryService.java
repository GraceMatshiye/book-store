package za.ac.tut.grpjoint.book.store.service;

import za.ac.tut.grpjoint.book.store.model.dto.CountryDTO;

public interface CountryService extends MyCrudService<CountryDTO, Integer> {}
