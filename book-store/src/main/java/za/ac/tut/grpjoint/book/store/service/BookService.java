package za.ac.tut.grpjoint.book.store.service;

import za.ac.tut.grpjoint.book.store.model.dto.BookDTO;

public interface BookService extends MyCrudService<BookDTO, Integer> {}
