package za.ac.tut.grpjoint.book.store.service.exception;

/**
 *
 * @author gracematshiye
 */
public class RecordNotUpdatedException extends BusinessException {

    /**
     * Creates a new instance of <code>RecordNotCreatedException</code> without
     * detail message.
     */
    public RecordNotUpdatedException() {
    }

    /**
     * Constructs an instance of <code>RecordNotCreatedException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public RecordNotUpdatedException(String msg) {
        super(msg);
    }
}
