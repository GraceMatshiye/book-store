package za.ac.tut.grpjoint.book.store.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.ac.tut.grpjoint.book.store.model.dao.SystemMenuDAO;
import za.ac.tut.grpjoint.book.store.model.dto.SystemMenuDTO;

@Component 
public class SystemMenuServiceBean extends AbstractCrudService<SystemMenuDTO, Integer, SystemMenuDAO>implements SystemMenuService {

    @Autowired
    public SystemMenuServiceBean(SystemMenuDAO dao) {
        super(dao);
    }
}
