package za.ac.tut.grpjoint.book.store.model.entity;

import za.ac.tut.grpjoint.book.store.model.dto.UsrDTO;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author gracematshiye
 */
@Entity
@Table(name = "USR_SESSION", catalog = "BOOK_STOREDB", schema = "", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"uuid"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UsrSession.findAll", query = "SELECT u FROM UsrSession u")
    , @NamedQuery(name = "UsrSession.findById", query = "SELECT u FROM UsrSession u WHERE u.id = :id")
    , @NamedQuery(name = "UsrSession.findByDateCreated", query = "SELECT u FROM UsrSession u WHERE u.dateCreated = :dateCreated")
    , @NamedQuery(name = "UsrSession.findByLastUpdated", query = "SELECT u FROM UsrSession u WHERE u.lastUpdated = :lastUpdated")
    , @NamedQuery(name = "UsrSession.findByActive", query = "SELECT u FROM UsrSession u WHERE u.active = :active")
    , @NamedQuery(name = "UsrSession.findByUuid", query = "SELECT u FROM UsrSession u WHERE u.uuid = :uuid")})
public class UsrSession implements Serializable, MyEntity {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "date_created", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreated;
    @Basic(optional = false)
    @NotNull
    @Column(name = "last_updated", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdated;
    @Basic(optional = false)
    @NotNull
    @Column(name = "active", nullable = false)
    private boolean active;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 36)
    @Column(name = "uuid", nullable = false, length = 36)
    private String uuid;
    @JoinColumn(name = "usr", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private UsrDTO usr;

    public UsrSession() {
    }

    public UsrSession(Integer id) {
        this.id = id;
    }

    public UsrSession(Integer id, Date dateCreated, Date lastUpdated, boolean active, String uuid) {
        this.id = id;
        this.dateCreated = dateCreated;
        this.lastUpdated = lastUpdated;
        this.active = active;
        this.uuid = uuid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public boolean getActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public UsrDTO getUsr() {
        return usr;
    }

    public void setUsr(UsrDTO usr) {
        this.usr = usr;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UsrSession)) {
            return false;
        }
        UsrSession other = (UsrSession) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "za.ac.tut.grpjoint.book.store.model.entity.UsrSession[ id=" + id + " ]";
    }
    
}
