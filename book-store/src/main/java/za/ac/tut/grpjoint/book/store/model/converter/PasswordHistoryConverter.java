package za.ac.tut.grpjoint.book.store.model.converter;

import org.modelmapper.ModelMapper;
import za.ac.tut.grpjoint.book.store.model.dto.PasswordHistoryDTO;
import za.ac.tut.grpjoint.book.store.model.entity.PasswordHistory;

public class PasswordHistoryConverter extends AbstractConverter<PasswordHistory, PasswordHistoryDTO> {

    @Override
    public PasswordHistory toEntity(PasswordHistoryDTO dto) {
        ModelMapper mm =  new ModelMapper();
        PasswordHistory entity = mm.map(dto, PasswordHistory.class);
        return entity;
    }

    @Override
    public PasswordHistoryDTO toDTO(PasswordHistory entity) {
        ModelMapper mm =  new ModelMapper();
        PasswordHistoryDTO dto = mm.map(entity, PasswordHistoryDTO.class);
        return dto;
    }
}
