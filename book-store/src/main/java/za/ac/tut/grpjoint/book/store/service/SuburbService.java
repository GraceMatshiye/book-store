package za.ac.tut.grpjoint.book.store.service;

import za.ac.tut.grpjoint.book.store.model.dto.SuburbDTO;

public interface SuburbService extends MyCrudService<SuburbDTO, Integer> {}
