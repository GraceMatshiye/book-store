package za.ac.tut.grpjoint.book.store.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.ac.tut.grpjoint.book.store.model.dao.CountryDAO;
import za.ac.tut.grpjoint.book.store.model.dto.CountryDTO;

@Component 
public class CountryServiceBean extends AbstractCrudService<CountryDTO, Integer, CountryDAO>implements CountryService {

    @Autowired
    public CountryServiceBean(CountryDAO dao) {
        super(dao);
    }
}
