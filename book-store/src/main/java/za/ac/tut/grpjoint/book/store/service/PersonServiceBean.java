package za.ac.tut.grpjoint.book.store.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.ac.tut.grpjoint.book.store.model.dao.PersonDAO;
import za.ac.tut.grpjoint.book.store.model.dto.PersonDTO;

@Component 
public class PersonServiceBean extends AbstractCrudService<PersonDTO, Integer, PersonDAO>implements PersonService {

    @Autowired
    public PersonServiceBean(PersonDAO dao) {
        super(dao);
    }
}
