/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.ac.tut.grpjoint.book.store.model.dto;

import lombok.Data;

/**
 *
 * @author gracematshiye
 */
@Data
public class RoleMenuDTO implements MyDTO {

    private Integer id;
    private String uuid;
    private SystemMenuDTO mnu;
    private UsrRoleDTO usrRole;

}
