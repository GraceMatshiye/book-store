package za.ac.tut.grpjoint.book.store.service;

import za.ac.tut.grpjoint.book.store.model.dto.CityDTO;

public interface CityService extends MyCrudService<CityDTO, Integer> {}
