/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.ac.tut.grpjoint.book.store.model.dto;

import lombok.Data;

/**
 *
 * @author gracematshiye
 */
@Data
public class PartnerAssociatePKDTO implements MyDTO {

    private int partner;
    private int associate;

    public PartnerAssociatePKDTO(int partner, int associate) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
