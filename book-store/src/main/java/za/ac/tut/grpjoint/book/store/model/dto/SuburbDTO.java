/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.ac.tut.grpjoint.book.store.model.dto;

import java.util.List;
import lombok.Data;

/**
 *
 * @author gracematshiye
 */
@Data
public class SuburbDTO implements MyDTO {

    private Integer id;
    private String name;
    private String municipality;
    private String streetCode;
    private String postalCode;
    private CityDTO city;
    private List<AddressDTO> addressList;

}
