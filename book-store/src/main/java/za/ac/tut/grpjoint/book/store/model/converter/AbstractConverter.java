package za.ac.tut.grpjoint.book.store.model.converter;

import java.util.ArrayList;
import java.util.List;
import lombok.NonNull;
import za.ac.tut.grpjoint.book.store.model.dto.MyDTO;
import za.ac.tut.grpjoint.book.store.model.entity.MyEntity;

/**
 *
 * @author gracematshiye
 */
public abstract class AbstractConverter<E extends MyEntity, D extends MyDTO> implements MyConverter<E, D> {

    @Override
    public List<E> toEntity(@NonNull List<D> dtos) {
        List<E> entities = new ArrayList<>();
        dtos.forEach((dto) -> entities.add(toEntity(dto)));
        return entities;
    }

    @Override
    public List<D> toDTO(@NonNull List<E> entities) {
        List<D> dtos = new ArrayList<>();
        entities.forEach(entity -> dtos.add(toDTO(entity)));
        return dtos;
    }

}
