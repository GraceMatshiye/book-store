package za.ac.tut.grpjoint.book.store.model.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.ac.tut.grpjoint.book.store.model.converter.RoleMenuConverter;
import za.ac.tut.grpjoint.book.store.model.dao.jpa.RoleMenuJpaDAO;
import za.ac.tut.grpjoint.book.store.model.dto.RoleMenuDTO;
import za.ac.tut.grpjoint.book.store.model.entity.RoleMenu;

@Component
public class RoleMenuDAO extends AbstractJpaDAO<RoleMenu, RoleMenuDTO, RoleMenuConverter, Integer, RoleMenuJpaDAO> {

    @Autowired
    public RoleMenuDAO(RoleMenuJpaDAO jpaRepo, RoleMenuConverter converter) {
        super(jpaRepo, converter);
    }
}
