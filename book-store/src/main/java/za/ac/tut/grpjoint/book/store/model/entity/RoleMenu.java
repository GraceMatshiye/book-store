package za.ac.tut.grpjoint.book.store.model.entity;

import za.ac.tut.grpjoint.book.store.model.dto.SystemMenuDTO;
import za.ac.tut.grpjoint.book.store.model.dto.UsrRoleDTO;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author gracematshiye
 */
@Entity
@Table(name = "ROLE_MENU", catalog = "BOOK_STOREDB", schema = "", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"uuid"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RoleMenu.findAll", query = "SELECT r FROM RoleMenu r")
    , @NamedQuery(name = "RoleMenu.findById", query = "SELECT r FROM RoleMenu r WHERE r.id = :id")
    , @NamedQuery(name = "RoleMenu.findByUuid", query = "SELECT r FROM RoleMenu r WHERE r.uuid = :uuid")})
public class RoleMenu implements Serializable, MyEntity {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 36)
    @Column(name = "uuid", nullable = false, length = 36)
    private String uuid;
    @JoinColumn(name = "mnu", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private SystemMenuDTO mnu;
    @JoinColumn(name = "usr_role", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private UsrRoleDTO usrRole;

    public RoleMenu() {
    }

    public RoleMenu(Integer id) {
        this.id = id;
    }

    public RoleMenu(Integer id, String uuid) {
        this.id = id;
        this.uuid = uuid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public SystemMenuDTO getMnu() {
        return mnu;
    }

    public void setMnu(SystemMenuDTO mnu) {
        this.mnu = mnu;
    }

    public UsrRoleDTO getUsrRole() {
        return usrRole;
    }

    public void setUsrRole(UsrRoleDTO usrRole) {
        this.usrRole = usrRole;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RoleMenu)) {
            return false;
        }
        RoleMenu other = (RoleMenu) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "za.ac.tut.grpjoint.book.store.model.entity.RoleMenu[ id=" + id + " ]";
    }
    
}
