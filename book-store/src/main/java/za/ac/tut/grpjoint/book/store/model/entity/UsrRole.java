package za.ac.tut.grpjoint.book.store.model.entity;

import za.ac.tut.grpjoint.book.store.model.dto.RoleMenuDTO;
import za.ac.tut.grpjoint.book.store.model.dto.PartnerDTO;
import za.ac.tut.grpjoint.book.store.model.dto.UsrDTO;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author gracematshiye
 */
@Entity
@Table(name = "USR_ROLE", catalog = "BOOK_STOREDB", schema = "", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"system_key"})
    , @UniqueConstraint(columnNames = {"uuid"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UsrRole.findAll", query = "SELECT u FROM UsrRole u")
    , @NamedQuery(name = "UsrRole.findById", query = "SELECT u FROM UsrRole u WHERE u.id = :id")
    , @NamedQuery(name = "UsrRole.findByName", query = "SELECT u FROM UsrRole u WHERE u.name = :name")
    , @NamedQuery(name = "UsrRole.findBySystemKey", query = "SELECT u FROM UsrRole u WHERE u.systemKey = :systemKey")
    , @NamedQuery(name = "UsrRole.findByDescription", query = "SELECT u FROM UsrRole u WHERE u.description = :description")
    , @NamedQuery(name = "UsrRole.findByRoleLevel", query = "SELECT u FROM UsrRole u WHERE u.roleLevel = :roleLevel")
    , @NamedQuery(name = "UsrRole.findByActive", query = "SELECT u FROM UsrRole u WHERE u.active = :active")
    , @NamedQuery(name = "UsrRole.findByUuid", query = "SELECT u FROM UsrRole u WHERE u.uuid = :uuid")})
public class UsrRole implements Serializable, MyEntity {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "name", nullable = false, length = 45)
    private String name;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "system_key", nullable = false, length = 45)
    private String systemKey;
    @Size(max = 45)
    @Column(name = "description", length = 45)
    private String description;
    @Basic(optional = false)
    @NotNull
    @Column(name = "role_level", nullable = false)
    private int roleLevel;
    @Basic(optional = false)
    @NotNull
    @Column(name = "active", nullable = false)
    private boolean active;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 36)
    @Column(name = "uuid", nullable = false, length = 36)
    private String uuid;
    @OneToMany(mappedBy = "usrRole")
    private List<UsrDTO> usrList;
    @JoinColumn(name = "partner", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private PartnerDTO partner;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "usrRole")
    private List<RoleMenuDTO> roleMenuList;

    public UsrRole() {
    }

    public UsrRole(Integer id) {
        this.id = id;
    }

    public UsrRole(Integer id, String name, String systemKey, int roleLevel, boolean active, String uuid) {
        this.id = id;
        this.name = name;
        this.systemKey = systemKey;
        this.roleLevel = roleLevel;
        this.active = active;
        this.uuid = uuid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSystemKey() {
        return systemKey;
    }

    public void setSystemKey(String systemKey) {
        this.systemKey = systemKey;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getRoleLevel() {
        return roleLevel;
    }

    public void setRoleLevel(int roleLevel) {
        this.roleLevel = roleLevel;
    }

    public boolean getActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @XmlTransient
    public List<UsrDTO> getUsrList() {
        return usrList;
    }

    public void setUsrList(List<UsrDTO> usrList) {
        this.usrList = usrList;
    }

    public PartnerDTO getPartner() {
        return partner;
    }

    public void setPartner(PartnerDTO partner) {
        this.partner = partner;
    }

    @XmlTransient
    public List<RoleMenuDTO> getRoleMenuList() {
        return roleMenuList;
    }

    public void setRoleMenuList(List<RoleMenuDTO> roleMenuList) {
        this.roleMenuList = roleMenuList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UsrRole)) {
            return false;
        }
        UsrRole other = (UsrRole) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "za.ac.tut.grpjoint.book.store.model.entity.UsrRole[ id=" + id + " ]";
    }
    
}
