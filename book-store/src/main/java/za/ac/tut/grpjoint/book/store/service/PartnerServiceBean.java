package za.ac.tut.grpjoint.book.store.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.ac.tut.grpjoint.book.store.model.dao.PartnerDAO;
import za.ac.tut.grpjoint.book.store.model.dto.PartnerDTO;

@Component 
public class PartnerServiceBean extends AbstractCrudService<PartnerDTO, Integer, PartnerDAO>implements PartnerService {

    @Autowired
    public PartnerServiceBean(PartnerDAO dao) {
        super(dao);
    }
}
