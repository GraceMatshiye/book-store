/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.ac.tut.grpjoint.book.store.model.dto;

import java.util.Date;
import lombok.Data;

/**
 *
 * @author gracematshiye
 */
@Data
public class UsrSessionDTO implements MyDTO {

    private Integer id;
    private Date dateCreated;
    private Date lastUpdated;
    private boolean active;
    private String uuid;
    private UsrDTO usr;

}
