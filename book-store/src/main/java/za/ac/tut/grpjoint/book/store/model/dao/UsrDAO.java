package za.ac.tut.grpjoint.book.store.model.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.ac.tut.grpjoint.book.store.model.converter.UsrConverter;
import za.ac.tut.grpjoint.book.store.model.dao.jpa.UsrJpaDAO;
import za.ac.tut.grpjoint.book.store.model.dto.UsrDTO;
import za.ac.tut.grpjoint.book.store.model.entity.Usr;

@Component
public class UsrDAO extends AbstractJpaDAO<Usr, UsrDTO, UsrConverter, Integer, UsrJpaDAO> {

    @Autowired
    public UsrDAO(UsrJpaDAO jpaRepo, UsrConverter converter) {
        super(jpaRepo, converter);
    }
}
