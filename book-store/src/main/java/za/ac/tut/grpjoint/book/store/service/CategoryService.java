package za.ac.tut.grpjoint.book.store.service;

import za.ac.tut.grpjoint.book.store.model.dto.CategoryDTO;

public interface CategoryService extends MyCrudService<CategoryDTO, Integer> {}
