package za.ac.tut.grpjoint.book.store.model.entity;

import za.ac.tut.grpjoint.book.store.model.dto.CityDTO;
import za.ac.tut.grpjoint.book.store.model.entity.MyEntity;
import za.ac.tut.grpjoint.book.store.model.dto.SuburbDTO;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Data;

/**
 *
 * @author gracematshiye
 */
@Data
@Entity
@Table(name = "ADDRESS", catalog = "BOOK_STOREDB", schema = "", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"uuid"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Address.findAll", query = "SELECT a FROM Address a")
    , @NamedQuery(name = "Address.findById", query = "SELECT a FROM Address a WHERE a.id = :id")
    , @NamedQuery(name = "Address.findByLine1", query = "SELECT a FROM Address a WHERE a.line1 = :line1")
    , @NamedQuery(name = "Address.findByLine2", query = "SELECT a FROM Address a WHERE a.line2 = :line2")
    , @NamedQuery(name = "Address.findByPostalCode", query = "SELECT a FROM Address a WHERE a.postalCode = :postalCode")
    , @NamedQuery(name = "Address.findByGpsLatitude", query = "SELECT a FROM Address a WHERE a.gpsLatitude = :gpsLatitude")
    , @NamedQuery(name = "Address.findByGpsLongitude", query = "SELECT a FROM Address a WHERE a.gpsLongitude = :gpsLongitude")
    , @NamedQuery(name = "Address.findByUuid", query = "SELECT a FROM Address a WHERE a.uuid = :uuid")})
public class Address implements Serializable, MyEntity {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 120)
    @Column(name = "line1", nullable = false, length = 120)
    private String line1;
    @Size(max = 120)
    @Column(name = "line2", length = 120)
    private String line2;
    @Size(max = 10)
    @Column(name = "postal_code", length = 10)
    private String postalCode;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "gps_latitude", precision = 10, scale = 6)
    private Float gpsLatitude;
    @Column(name = "gps_longitude", precision = 10, scale = 6)
    private Float gpsLongitude;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 36)
    @Column(name = "uuid", nullable = false, length = 36)
    private String uuid;
    @JoinColumn(name = "city", referencedColumnName = "id")
    @ManyToOne
    private CityDTO city;
    @JoinColumn(name = "suburb", referencedColumnName = "id")
    @ManyToOne
    private SuburbDTO suburb;

}
