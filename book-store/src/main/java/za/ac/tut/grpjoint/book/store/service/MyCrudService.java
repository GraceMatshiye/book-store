package za.ac.tut.grpjoint.book.store.service;

import java.io.Serializable;
import java.util.List;
import za.ac.tut.grpjoint.book.store.model.dto.MyDTO;
import za.ac.tut.grpjoint.book.store.service.exception.RecordNotCreatedException;
import za.ac.tut.grpjoint.book.store.service.exception.RecordNotUpdatedException;

/**
 *
 * @author gracematshiye
 */
public interface MyCrudService<D extends MyDTO, ID extends Serializable> {

    List<D> findAll();

    D findById(ID id);

    D save(D dto) throws RecordNotCreatedException;

    void save(List<D> dtoList) throws RecordNotCreatedException;

    D update(D dto) throws RecordNotUpdatedException;

    void delete(D dto) throws RecordNotUpdatedException;    
}
