package za.ac.tut.grpjoint.book.store.service;

import za.ac.tut.grpjoint.book.store.model.dto.PartnerDTO;

public interface PartnerService extends MyCrudService<PartnerDTO, Integer> {}
