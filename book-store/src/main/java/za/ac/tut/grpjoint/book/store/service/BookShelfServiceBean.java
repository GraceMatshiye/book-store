package za.ac.tut.grpjoint.book.store.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.ac.tut.grpjoint.book.store.model.dao.BookShelfDAO;
import za.ac.tut.grpjoint.book.store.model.dto.BookShelfDTO;

@Component
public class BookShelfServiceBean extends AbstractCrudService<BookShelfDTO, Integer, BookShelfDAO>implements BookShelfService {

    @Autowired
    public BookShelfServiceBean(BookShelfDAO dao) {
        super(dao);
    }
}
