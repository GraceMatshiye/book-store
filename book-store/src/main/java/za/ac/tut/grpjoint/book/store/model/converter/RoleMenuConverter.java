package za.ac.tut.grpjoint.book.store.model.converter;

import org.modelmapper.ModelMapper;
import za.ac.tut.grpjoint.book.store.model.dto.RoleMenuDTO;
import za.ac.tut.grpjoint.book.store.model.entity.RoleMenu;

public class RoleMenuConverter extends AbstractConverter<RoleMenu, RoleMenuDTO> {

    @Override
    public RoleMenu toEntity(RoleMenuDTO dto) {
        ModelMapper mm =  new ModelMapper();
        RoleMenu entity = mm.map(dto, RoleMenu.class);
        return entity;
    }

    @Override
    public RoleMenuDTO toDTO(RoleMenu entity) {
        ModelMapper mm =  new ModelMapper();
        RoleMenuDTO dto = mm.map(entity, RoleMenuDTO.class);
        return dto;
    }
}
