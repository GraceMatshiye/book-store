package za.ac.tut.grpjoint.book.store.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.ac.tut.grpjoint.book.store.model.dao.BookDAO;
import za.ac.tut.grpjoint.book.store.model.dto.BookDTO;

@Component 
public class BookServiceBean extends AbstractCrudService<BookDTO, Integer, BookDAO>implements BookService {

    @Autowired
    public BookServiceBean(BookDAO dao) {
        super(dao);
    }
}
