package za.ac.tut.grpjoint.book.store.service;

import za.ac.tut.grpjoint.book.store.model.dto.PartnerAssociateDTO;

public interface PartnerAssociateService extends MyCrudService<PartnerAssociateDTO, Integer> {}
