package za.ac.tut.grpjoint.book.store.model.dao.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import za.ac.tut.grpjoint.book.store.model.entity.PasswordHistory;

public interface PasswordHistoryJpaDAO extends JpaRepository<PasswordHistory, Integer> {
}
