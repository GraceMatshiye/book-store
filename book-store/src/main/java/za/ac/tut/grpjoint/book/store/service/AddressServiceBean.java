package za.ac.tut.grpjoint.book.store.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.ac.tut.grpjoint.book.store.model.dao.AddressDAO;
import za.ac.tut.grpjoint.book.store.model.dto.AddressDTO;

@Component 
public class AddressServiceBean extends AbstractCrudService<AddressDTO, Integer, AddressDAO>implements AddressService {

    @Autowired
    public AddressServiceBean(AddressDAO dao) {
        super(dao);
    }
}
