package za.ac.tut.grpjoint.book.store.model.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.ac.tut.grpjoint.book.store.model.converter.PersonConverter;
import za.ac.tut.grpjoint.book.store.model.dao.jpa.PersonJpaDAO;
import za.ac.tut.grpjoint.book.store.model.dto.PersonDTO;
import za.ac.tut.grpjoint.book.store.model.entity.Person;

@Component
public class PersonDAO extends AbstractJpaDAO<Person, PersonDTO, PersonConverter, Integer, PersonJpaDAO> {

    @Autowired
    public PersonDAO(PersonJpaDAO jpaRepo, PersonConverter converter) {
        super(jpaRepo, converter);
    }
}
