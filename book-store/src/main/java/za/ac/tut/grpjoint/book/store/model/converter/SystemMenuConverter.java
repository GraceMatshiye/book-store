package za.ac.tut.grpjoint.book.store.model.converter;

import org.modelmapper.ModelMapper;
import za.ac.tut.grpjoint.book.store.model.dto.SystemMenuDTO;
import za.ac.tut.grpjoint.book.store.model.entity.SystemMenu;

public class SystemMenuConverter extends AbstractConverter<SystemMenu, SystemMenuDTO> {

    @Override
    public SystemMenu toEntity(SystemMenuDTO dto) {
        ModelMapper mm =  new ModelMapper();
        SystemMenu entity = mm.map(dto, SystemMenu.class);
        return entity;
    }

    @Override
    public SystemMenuDTO toDTO(SystemMenu entity) {
        ModelMapper mm =  new ModelMapper();
        SystemMenuDTO dto = mm.map(entity, SystemMenuDTO.class);
        return dto;
    }
}
