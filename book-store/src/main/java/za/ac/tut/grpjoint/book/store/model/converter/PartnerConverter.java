package za.ac.tut.grpjoint.book.store.model.converter;

import org.modelmapper.ModelMapper;
import za.ac.tut.grpjoint.book.store.model.dto.PartnerDTO;
import za.ac.tut.grpjoint.book.store.model.entity.Partner;

public class PartnerConverter extends AbstractConverter<Partner, PartnerDTO> {

    @Override
    public Partner toEntity(PartnerDTO dto) {
        ModelMapper mm =  new ModelMapper();
        Partner entity = mm.map(dto, Partner.class);
        return entity;
    }

    @Override
    public PartnerDTO toDTO(Partner entity) {
        ModelMapper mm =  new ModelMapper();
        PartnerDTO dto = mm.map(entity, PartnerDTO.class);
        return dto;
    }
}
