package za.ac.tut.grpjoint.book.store.model.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.ac.tut.grpjoint.book.store.model.converter.AddressConverter;
import za.ac.tut.grpjoint.book.store.model.dao.jpa.AddressJpaDAO;
import za.ac.tut.grpjoint.book.store.model.dto.AddressDTO;
import za.ac.tut.grpjoint.book.store.model.entity.Address;

@Component
public class AddressDAO extends AbstractJpaDAO<Address, AddressDTO, AddressConverter, Integer, AddressJpaDAO> {

    @Autowired
    public AddressDAO(AddressJpaDAO jpaRepo, AddressConverter converter) {
        super(jpaRepo, converter);
    }
    
}
