package za.ac.tut.grpjoint.book.store.model.converter;

import org.modelmapper.ModelMapper;
import za.ac.tut.grpjoint.book.store.model.dto.CategoryDTO;
import za.ac.tut.grpjoint.book.store.model.entity.Category;

public class CategoryConverter extends AbstractConverter<Category, CategoryDTO> {

     @Override
    public Category toEntity(CategoryDTO dto) {
        ModelMapper mm =  new ModelMapper();
        Category entity = mm.map(dto, Category.class);
        return entity;
    }

    @Override
    public CategoryDTO toDTO(Category entity) {
        ModelMapper mm =  new ModelMapper();
        CategoryDTO dto = mm.map(entity, CategoryDTO.class);
        return dto;
    }
}
