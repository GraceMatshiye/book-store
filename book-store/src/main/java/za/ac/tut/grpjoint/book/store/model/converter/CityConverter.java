package za.ac.tut.grpjoint.book.store.model.converter;

import org.modelmapper.ModelMapper;
import za.ac.tut.grpjoint.book.store.model.dto.CityDTO;
import za.ac.tut.grpjoint.book.store.model.entity.City;

public class CityConverter extends AbstractConverter<City, CityDTO> {

   @Override
    public City toEntity(CityDTO dto) {
        ModelMapper mm =  new ModelMapper();
        City entity = mm.map(dto, City.class);
        return entity;
    }

    @Override
    public CityDTO toDTO(City entity) {
        ModelMapper mm =  new ModelMapper();
        CityDTO dto = mm.map(entity, CityDTO.class);
        return dto;
    }
}
