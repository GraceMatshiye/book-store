package za.ac.tut.grpjoint.book.store.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.ac.tut.grpjoint.book.store.model.dao.PasswordHistoryDAO;
import za.ac.tut.grpjoint.book.store.model.dto.PasswordHistoryDTO;

@Component
public class PasswordHistoryServiceBean extends AbstractCrudService<PasswordHistoryDTO, Integer, PasswordHistoryDAO>implements PasswordHistoryService {

    @Autowired
    public PasswordHistoryServiceBean(PasswordHistoryDAO dao) {
        super(dao);
    }
}
