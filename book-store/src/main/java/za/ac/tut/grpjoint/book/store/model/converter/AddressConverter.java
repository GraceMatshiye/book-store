package za.ac.tut.grpjoint.book.store.model.converter;

import org.modelmapper.ModelMapper;
import za.ac.tut.grpjoint.book.store.model.dto.AddressDTO;
import za.ac.tut.grpjoint.book.store.model.entity.Address;

public class AddressConverter extends AbstractConverter<Address, AddressDTO> {

   @Override
    public Address toEntity(AddressDTO dto) {
        ModelMapper mm =  new ModelMapper();
        Address entity = mm.map(dto, Address.class);
        return entity;
    }

    @Override
    public AddressDTO toDTO(Address entity) {
        ModelMapper mm =  new ModelMapper();
        AddressDTO dto = mm.map(entity, AddressDTO.class);
        return dto;
    }
}
