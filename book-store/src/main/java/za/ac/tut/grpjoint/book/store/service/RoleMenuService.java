package za.ac.tut.grpjoint.book.store.service;

import za.ac.tut.grpjoint.book.store.model.dto.RoleMenuDTO;

public interface RoleMenuService extends MyCrudService<RoleMenuDTO, Integer> {}
