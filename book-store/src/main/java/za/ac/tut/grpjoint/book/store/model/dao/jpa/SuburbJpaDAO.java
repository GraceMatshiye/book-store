package za.ac.tut.grpjoint.book.store.model.dao.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import za.ac.tut.grpjoint.book.store.model.entity.Suburb;

public interface SuburbJpaDAO extends JpaRepository<Suburb, Integer> {
}
