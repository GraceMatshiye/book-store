package za.ac.tut.grpjoint.book.store.model.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.ac.tut.grpjoint.book.store.model.converter.SystemMenuConverter;
import za.ac.tut.grpjoint.book.store.model.dao.jpa.SystemMenuJpaDAO;
import za.ac.tut.grpjoint.book.store.model.dto.SystemMenuDTO;
import za.ac.tut.grpjoint.book.store.model.entity.SystemMenu;

@Component
public class SystemMenuDAO extends AbstractJpaDAO<SystemMenu, SystemMenuDTO, SystemMenuConverter, Integer, SystemMenuJpaDAO> {

    @Autowired
    public SystemMenuDAO(SystemMenuJpaDAO jpaRepo, SystemMenuConverter converter) {
        super(jpaRepo, converter);
    }
}
