package za.ac.tut.grpjoint.book.store.model.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.ac.tut.grpjoint.book.store.model.converter.UsrSessionConverter;
import za.ac.tut.grpjoint.book.store.model.dao.jpa.UsrSessionJpaDAO;
import za.ac.tut.grpjoint.book.store.model.dto.UsrSessionDTO;
import za.ac.tut.grpjoint.book.store.model.entity.UsrSession;

@Component
public class UsrSessionDAO extends AbstractJpaDAO<UsrSession, UsrSessionDTO, UsrSessionConverter, Integer, UsrSessionJpaDAO> {

    @Autowired
    public UsrSessionDAO(UsrSessionJpaDAO jpaRepo, UsrSessionConverter converter) {
        super(jpaRepo, converter);
    }
}
