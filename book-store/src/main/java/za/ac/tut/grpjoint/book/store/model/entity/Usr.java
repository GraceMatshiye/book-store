package za.ac.tut.grpjoint.book.store.model.entity;

import za.ac.tut.grpjoint.book.store.model.dto.PasswordHistoryDTO;
import za.ac.tut.grpjoint.book.store.model.dto.PersonDTO;
import za.ac.tut.grpjoint.book.store.model.dto.BookShelfDTO;
import za.ac.tut.grpjoint.book.store.model.dto.UsrRoleDTO;
import za.ac.tut.grpjoint.book.store.model.dto.PartnerAssociateDTO;
import za.ac.tut.grpjoint.book.store.model.dto.UsrSessionDTO;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author gracematshiye
 */
@Entity
@Table(name = "USR", catalog = "BOOK_STOREDB", schema = "", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"username"})
    , @UniqueConstraint(columnNames = {"uuid"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Usr.findAll", query = "SELECT u FROM Usr u")
    , @NamedQuery(name = "Usr.findById", query = "SELECT u FROM Usr u WHERE u.id = :id")
    , @NamedQuery(name = "Usr.findByUsername", query = "SELECT u FROM Usr u WHERE u.username = :username")
    , @NamedQuery(name = "Usr.findByLastLoginDate", query = "SELECT u FROM Usr u WHERE u.lastLoginDate = :lastLoginDate")
    , @NamedQuery(name = "Usr.findBySuspended", query = "SELECT u FROM Usr u WHERE u.suspended = :suspended")
    , @NamedQuery(name = "Usr.findBySuspendedReason", query = "SELECT u FROM Usr u WHERE u.suspendedReason = :suspendedReason")
    , @NamedQuery(name = "Usr.findByPasswordChangeRequired", query = "SELECT u FROM Usr u WHERE u.passwordChangeRequired = :passwordChangeRequired")
    , @NamedQuery(name = "Usr.findByPasswordChangeDate", query = "SELECT u FROM Usr u WHERE u.passwordChangeDate = :passwordChangeDate")
    , @NamedQuery(name = "Usr.findByUuid", query = "SELECT u FROM Usr u WHERE u.uuid = :uuid")})
public class Usr implements Serializable, MyEntity {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "username", nullable = false, length = 45)
    private String username;
    @Column(name = "last_login_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastLoginDate;
    @Column(name = "suspended")
    private Boolean suspended;
    @Size(max = 450)
    @Column(name = "suspended_reason", length = 450)
    private String suspendedReason;
    @Basic(optional = false)
    @NotNull
    @Column(name = "password_change_required", nullable = false)
    private boolean passwordChangeRequired;
    @Column(name = "password_change_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date passwordChangeDate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 36)
    @Column(name = "uuid", nullable = false, length = 36)
    private String uuid;
    @OneToMany(mappedBy = "createdBy")
    private List<PartnerAssociateDTO> partnerAssociateList;
    @JoinColumn(name = "person", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private PersonDTO person;
    @JoinColumn(name = "usr_role", referencedColumnName = "id")
    @ManyToOne
    private UsrRoleDTO usrRole;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "seller")
    private List<BookShelfDTO> bookShelfList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "usr")
    private List<UsrSessionDTO> usrSessionList;
    @OneToMany(mappedBy = "usr")
    private List<PasswordHistoryDTO> passwordHistoryList;

    public Usr() {
    }

    public Usr(Integer id) {
        this.id = id;
    }

    public Usr(Integer id, String username, boolean passwordChangeRequired, String uuid) {
        this.id = id;
        this.username = username;
        this.passwordChangeRequired = passwordChangeRequired;
        this.uuid = uuid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getLastLoginDate() {
        return lastLoginDate;
    }

    public void setLastLoginDate(Date lastLoginDate) {
        this.lastLoginDate = lastLoginDate;
    }

    public Boolean getSuspended() {
        return suspended;
    }

    public void setSuspended(Boolean suspended) {
        this.suspended = suspended;
    }

    public String getSuspendedReason() {
        return suspendedReason;
    }

    public void setSuspendedReason(String suspendedReason) {
        this.suspendedReason = suspendedReason;
    }

    public boolean getPasswordChangeRequired() {
        return passwordChangeRequired;
    }

    public void setPasswordChangeRequired(boolean passwordChangeRequired) {
        this.passwordChangeRequired = passwordChangeRequired;
    }

    public Date getPasswordChangeDate() {
        return passwordChangeDate;
    }

    public void setPasswordChangeDate(Date passwordChangeDate) {
        this.passwordChangeDate = passwordChangeDate;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @XmlTransient
    public List<PartnerAssociateDTO> getPartnerAssociateList() {
        return partnerAssociateList;
    }

    public void setPartnerAssociateList(List<PartnerAssociateDTO> partnerAssociateList) {
        this.partnerAssociateList = partnerAssociateList;
    }

    public PersonDTO getPerson() {
        return person;
    }

    public void setPerson(PersonDTO person) {
        this.person = person;
    }

    public UsrRoleDTO getUsrRole() {
        return usrRole;
    }

    public void setUsrRole(UsrRoleDTO usrRole) {
        this.usrRole = usrRole;
    }

    @XmlTransient
    public List<BookShelfDTO> getBookShelfList() {
        return bookShelfList;
    }

    public void setBookShelfList(List<BookShelfDTO> bookShelfList) {
        this.bookShelfList = bookShelfList;
    }

    @XmlTransient
    public List<UsrSessionDTO> getUsrSessionList() {
        return usrSessionList;
    }

    public void setUsrSessionList(List<UsrSessionDTO> usrSessionList) {
        this.usrSessionList = usrSessionList;
    }

    @XmlTransient
    public List<PasswordHistoryDTO> getPasswordHistoryList() {
        return passwordHistoryList;
    }

    public void setPasswordHistoryList(List<PasswordHistoryDTO> passwordHistoryList) {
        this.passwordHistoryList = passwordHistoryList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Usr)) {
            return false;
        }
        Usr other = (Usr) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "za.ac.tut.grpjoint.book.store.model.entity.Usr[ id=" + id + " ]";
    }
    
}
