package za.ac.tut.grpjoint.book.store.model.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.ac.tut.grpjoint.book.store.model.converter.BookShelfConverter;
import za.ac.tut.grpjoint.book.store.model.dao.jpa.BookShelfJpaDAO;
import za.ac.tut.grpjoint.book.store.model.dto.BookShelfDTO;
import za.ac.tut.grpjoint.book.store.model.entity.BookShelf;

@Component 
public class BookShelfDAO extends AbstractJpaDAO<BookShelf, BookShelfDTO, BookShelfConverter, Integer, BookShelfJpaDAO> {

    @Autowired
    public BookShelfDAO(BookShelfJpaDAO jpaRepo, BookShelfConverter converter) {
        super(jpaRepo, converter);
    }
}
