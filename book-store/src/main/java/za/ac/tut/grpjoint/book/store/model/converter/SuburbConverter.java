package za.ac.tut.grpjoint.book.store.model.converter;

import org.modelmapper.ModelMapper;
import za.ac.tut.grpjoint.book.store.model.dto.SuburbDTO;
import za.ac.tut.grpjoint.book.store.model.entity.Suburb;

public class SuburbConverter extends AbstractConverter<Suburb, SuburbDTO> {

    @Override
    public Suburb toEntity(SuburbDTO dto) {
        ModelMapper mm =  new ModelMapper();
        Suburb entity = mm.map(dto, Suburb.class);
        return entity;
    }

    @Override
    public SuburbDTO toDTO(Suburb entity) {
        ModelMapper mm =  new ModelMapper();
        SuburbDTO dto = mm.map(entity, SuburbDTO.class);
        return dto;
    }
}
