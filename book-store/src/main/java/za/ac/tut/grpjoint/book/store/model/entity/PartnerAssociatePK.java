package za.ac.tut.grpjoint.book.store.model.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author gracematshiye
 */
@Embeddable
public class PartnerAssociatePK implements Serializable, MyEntity {

    @Basic(optional = false)
    @NotNull
    @Column(name = "partner", nullable = false)
    private int partner;
    @Basic(optional = false)
    @NotNull
    @Column(name = "associate", nullable = false)
    private int associate;

    public PartnerAssociatePK() {
    }

    public PartnerAssociatePK(int partner, int associate) {
        this.partner = partner;
        this.associate = associate;
    }

    public int getPartner() {
        return partner;
    }

    public void setPartner(int partner) {
        this.partner = partner;
    }

    public int getAssociate() {
        return associate;
    }

    public void setAssociate(int associate) {
        this.associate = associate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) partner;
        hash += (int) associate;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PartnerAssociatePK)) {
            return false;
        }
        PartnerAssociatePK other = (PartnerAssociatePK) object;
        if (this.partner != other.partner) {
            return false;
        }
        if (this.associate != other.associate) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "za.ac.tut.grpjoint.book.store.model.entity.PartnerAssociatePK[ partner=" + partner + ", associate=" + associate + " ]";
    }
    
}
