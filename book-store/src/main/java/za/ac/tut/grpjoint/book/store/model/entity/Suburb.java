package za.ac.tut.grpjoint.book.store.model.entity;

import za.ac.tut.grpjoint.book.store.model.dto.AddressDTO;
import za.ac.tut.grpjoint.book.store.model.dto.CityDTO;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author gracematshiye
 */
@Entity
@Table(name = "SUBURB", catalog = "BOOK_STOREDB", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Suburb.findAll", query = "SELECT s FROM Suburb s")
    , @NamedQuery(name = "Suburb.findById", query = "SELECT s FROM Suburb s WHERE s.id = :id")
    , @NamedQuery(name = "Suburb.findByName", query = "SELECT s FROM Suburb s WHERE s.name = :name")
    , @NamedQuery(name = "Suburb.findByMunicipality", query = "SELECT s FROM Suburb s WHERE s.municipality = :municipality")
    , @NamedQuery(name = "Suburb.findByStreetCode", query = "SELECT s FROM Suburb s WHERE s.streetCode = :streetCode")
    , @NamedQuery(name = "Suburb.findByPostalCode", query = "SELECT s FROM Suburb s WHERE s.postalCode = :postalCode")})
public class Suburb implements Serializable, MyEntity {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 150)
    @Column(name = "name", nullable = false, length = 150)
    private String name;
    @Size(max = 90)
    @Column(name = "municipality", length = 90)
    private String municipality;
    @Size(max = 45)
    @Column(name = "street_code", length = 45)
    private String streetCode;
    @Size(max = 45)
    @Column(name = "postal_code", length = 45)
    private String postalCode;
    @JoinColumn(name = "city", referencedColumnName = "id")
    @ManyToOne
    private CityDTO city;
    @OneToMany(mappedBy = "suburb")
    private List<AddressDTO> addressList;

    public Suburb() {
    }

    public Suburb(Integer id) {
        this.id = id;
    }

    public Suburb(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMunicipality() {
        return municipality;
    }

    public void setMunicipality(String municipality) {
        this.municipality = municipality;
    }

    public String getStreetCode() {
        return streetCode;
    }

    public void setStreetCode(String streetCode) {
        this.streetCode = streetCode;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public CityDTO getCity() {
        return city;
    }

    public void setCity(CityDTO city) {
        this.city = city;
    }

    @XmlTransient
    public List<AddressDTO> getAddressList() {
        return addressList;
    }

    public void setAddressList(List<AddressDTO> addressList) {
        this.addressList = addressList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Suburb)) {
            return false;
        }
        Suburb other = (Suburb) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "za.ac.tut.grpjoint.book.store.model.entity.Suburb[ id=" + id + " ]";
    }
    
}
