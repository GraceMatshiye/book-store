package za.ac.tut.grpjoint.book.store.service;

import java.io.Serializable;
import java.util.List;
import za.ac.tut.grpjoint.book.store.model.dao.AbstractJpaDAO;
import za.ac.tut.grpjoint.book.store.model.dto.MyDTO;
import za.ac.tut.grpjoint.book.store.service.exception.RecordNotCreatedException;
import za.ac.tut.grpjoint.book.store.service.exception.RecordNotUpdatedException;
/**
 *
 * @author gracematshiyeg
 */
public class AbstractCrudService<DTO extends MyDTO, ID extends Serializable, DAO extends AbstractJpaDAO<?, DTO, ?, ID, ?>> implements MyCrudService<DTO, ID>{

    private final DAO dao;

    public AbstractCrudService(DAO dao) {
        this.dao = dao;
    }    
    
    @Override
    public List<DTO> findAll() {
        return dao.findAll();
    }

    @Override
    public DTO findById(ID id) {
//        re
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public DTO save(DTO dto) throws RecordNotCreatedException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void save(List<DTO> dtoList) throws RecordNotCreatedException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public DTO update(DTO dto) throws RecordNotUpdatedException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(DTO dto) throws RecordNotUpdatedException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
