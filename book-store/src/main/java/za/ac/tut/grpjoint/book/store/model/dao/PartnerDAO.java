package za.ac.tut.grpjoint.book.store.model.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.ac.tut.grpjoint.book.store.model.converter.PartnerConverter;
import za.ac.tut.grpjoint.book.store.model.dao.jpa.PartnerJpaDAO;
import za.ac.tut.grpjoint.book.store.model.dto.PartnerDTO;
import za.ac.tut.grpjoint.book.store.model.entity.Partner;

@Component
public class PartnerDAO extends AbstractJpaDAO<Partner, PartnerDTO, PartnerConverter, Integer, PartnerJpaDAO> {

    @Autowired
    public PartnerDAO(PartnerJpaDAO jpaRepo, PartnerConverter converter) {
        super(jpaRepo, converter);
    }
}
