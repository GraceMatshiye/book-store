package za.ac.tut.grpjoint.book.store.service;

import za.ac.tut.grpjoint.book.store.model.dto.UsrSessionDTO;

public interface UsrSessionService extends MyCrudService<UsrSessionDTO, Integer> {}
