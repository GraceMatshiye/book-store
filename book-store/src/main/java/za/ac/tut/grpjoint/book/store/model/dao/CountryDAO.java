package za.ac.tut.grpjoint.book.store.model.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.ac.tut.grpjoint.book.store.model.converter.CountryConverter;
import za.ac.tut.grpjoint.book.store.model.dao.jpa.CountryJpaDAO;
import za.ac.tut.grpjoint.book.store.model.dto.CountryDTO;
import za.ac.tut.grpjoint.book.store.model.entity.Country;

@Component 
public class CountryDAO extends AbstractJpaDAO<Country, CountryDTO, CountryConverter, Integer, CountryJpaDAO> {
    
    @Autowired
    public CountryDAO(CountryJpaDAO jpaRepo, CountryConverter converter) {
        super(jpaRepo, converter);
    }
}
