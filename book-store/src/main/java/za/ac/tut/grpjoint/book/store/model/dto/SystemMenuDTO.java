/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.ac.tut.grpjoint.book.store.model.dto;

import java.util.List;
import lombok.Data;

/**
 *
 * @author gracematshiye
 */
@Data
public class SystemMenuDTO implements MyDTO {

    private Integer id;
    private String displayName;
    private String iconClass;
    private String href;
    private String description;
    private boolean active;
    private int menuOrder;
    private int systemModule;
    private String uuid;
    private List<RoleMenuDTO> roleMenuList;
    private List<SystemMenuDTO> systemMenuList;
    private SystemMenuDTO parent;
    private PartnerDTO partner;

}
