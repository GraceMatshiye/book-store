package za.ac.tut.grpjoint.book.store.model.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.ac.tut.grpjoint.book.store.model.converter.CityConverter;
import za.ac.tut.grpjoint.book.store.model.dao.jpa.CityJpaDAO;
import za.ac.tut.grpjoint.book.store.model.dto.CityDTO;
import za.ac.tut.grpjoint.book.store.model.entity.City;

@Component
public class CityDAO extends AbstractJpaDAO<City, CityDTO, CityConverter, Integer, CityJpaDAO> {

    @Autowired
    public CityDAO(CityJpaDAO jpaRepo, CityConverter converter) {
        super(jpaRepo, converter);
    }
}
