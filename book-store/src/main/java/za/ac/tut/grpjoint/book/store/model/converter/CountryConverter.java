package za.ac.tut.grpjoint.book.store.model.converter;

import org.modelmapper.ModelMapper;
import za.ac.tut.grpjoint.book.store.model.dto.CountryDTO;
import za.ac.tut.grpjoint.book.store.model.entity.Country;

public class CountryConverter extends AbstractConverter<Country, CountryDTO> {

     @Override
    public Country toEntity(CountryDTO dto) {
        ModelMapper mm =  new ModelMapper();
        Country entity = mm.map(dto, Country.class);
        return entity;
    }

    @Override
    public CountryDTO toDTO(Country entity) {
        ModelMapper mm =  new ModelMapper();
        CountryDTO dto = mm.map(entity, CountryDTO.class);
        return dto;
    }
}
