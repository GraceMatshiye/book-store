package za.ac.tut.grpjoint.book.store.service.exception;

/**
 *
 * @author gracematshiye
 */
public class RecordNotCreatedException extends BusinessException {

    /**
     * Creates a new instance of <code>RecordNotCreatedException</code> without
     * detail message.
     */
    public RecordNotCreatedException() {
    }

    /**
     * Constructs an instance of <code>RecordNotCreatedException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public RecordNotCreatedException(String msg) {
        super(msg);
    }
}
