package za.ac.tut.grpjoint.book.store.service;

import za.ac.tut.grpjoint.book.store.model.dto.PasswordHistoryDTO;

public interface PasswordHistoryService extends MyCrudService<PasswordHistoryDTO, Integer> {}
