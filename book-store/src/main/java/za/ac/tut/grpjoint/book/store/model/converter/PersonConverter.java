package za.ac.tut.grpjoint.book.store.model.converter;

import org.modelmapper.ModelMapper;
import za.ac.tut.grpjoint.book.store.model.dto.PersonDTO;
import za.ac.tut.grpjoint.book.store.model.entity.Person;

public class PersonConverter extends AbstractConverter<Person, PersonDTO> {

    @Override
    public Person toEntity(PersonDTO dto) {
        ModelMapper mm =  new ModelMapper();
        Person entity = mm.map(dto, Person.class);
        return entity;
    }

    @Override
    public PersonDTO toDTO(Person entity) {
        ModelMapper mm =  new ModelMapper();
        PersonDTO dto = mm.map(entity, PersonDTO.class);
        return dto;
    }
}
