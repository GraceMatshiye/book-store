package za.ac.tut.grpjoint.book.store.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.ac.tut.grpjoint.book.store.model.dao.SuburbDAO;
import za.ac.tut.grpjoint.book.store.model.dto.SuburbDTO;

@Component 
public class SuburbServiceBean extends AbstractCrudService< SuburbDTO, Integer,  SuburbDAO>implements  SuburbService {

    @Autowired
    public SuburbServiceBean(SuburbDAO dao) {
        super(dao);
    }
}
