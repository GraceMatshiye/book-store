package za.ac.tut.grpjoint.book.store.model.dao.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import za.ac.tut.grpjoint.book.store.model.entity.BookShelf;

public interface BookShelfJpaDAO extends JpaRepository<BookShelf, Integer> {
}
