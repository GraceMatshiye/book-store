package za.ac.tut.grpjoint.book.store.model.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.ac.tut.grpjoint.book.store.model.converter.PasswordHistoryConverter;
import za.ac.tut.grpjoint.book.store.model.dao.jpa.PasswordHistoryJpaDAO;
import za.ac.tut.grpjoint.book.store.model.dto.PasswordHistoryDTO;
import za.ac.tut.grpjoint.book.store.model.entity.PasswordHistory;

@Component
public class PasswordHistoryDAO extends AbstractJpaDAO<PasswordHistory, PasswordHistoryDTO, PasswordHistoryConverter, Integer, PasswordHistoryJpaDAO> {

    @Autowired
    public PasswordHistoryDAO(PasswordHistoryJpaDAO jpaRepo, PasswordHistoryConverter converter) {
        super(jpaRepo, converter);
    }
}
