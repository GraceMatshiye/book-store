package za.ac.tut.grpjoint.book.store.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.ac.tut.grpjoint.book.store.model.dao.PartnerAssociateDAO;
import za.ac.tut.grpjoint.book.store.model.dto.PartnerAssociateDTO;

@Component
public class PartnerAssociateServiceBean extends AbstractCrudService<PartnerAssociateDTO, Integer, PartnerAssociateDAO>implements PartnerAssociateService {

    @Autowired
    public PartnerAssociateServiceBean(PartnerAssociateDAO dao) {
        super(dao);
    }
}
