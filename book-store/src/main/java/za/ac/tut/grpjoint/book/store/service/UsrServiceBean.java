package za.ac.tut.grpjoint.book.store.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.ac.tut.grpjoint.book.store.model.dao.UsrDAO;
import za.ac.tut.grpjoint.book.store.model.dto.UsrDTO;

@Component 
public class UsrServiceBean extends AbstractCrudService<UsrDTO, Integer, UsrDAO>implements UsrService {

    @Autowired
    public UsrServiceBean(UsrDAO dao) {
        super(dao);
    }
}
